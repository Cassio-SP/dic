import std.stdio;
import std.process;
import std.random;
import std.datetime;
import std.algorithm;

int rand(int x) {
	auto r = uniform(0,x+1);
	return r;
}

int Ti;
int Tf;
int Td;

void main() {

	auto tv = Clock.currTime;

	immutable max = 10_000_000;

	int [] tabela;
	tabela.length = max;
	writeln("\n\n\tGerando matriz com ",max," posicoes....\n ");
	int counter = 0;
	while (counter < tabela.length) {
		tabela[counter] = rand(max);
	  // write(tabela[counter],"\t");
		++counter;
	}
	tv = Clock.currTime;
	Ti=tv.second;
	write("\n\n\tHorario inicial : ",
	      tv.hour,":",
	      tv.minute,":",
	      tv.second,"\n");
	writeln("\n\tclasificando....");
	//=====================
		sort(tabela);
	//=====================
	tv = Clock.currTime;
	Tf=tv.second;
	Td=Tf - Ti;
	if (Td < 0) Td = 60 + Td;
	writeln("\tclasificada em ",Td," segundos!\n");
	writeln("\tHorario Final: ",
	        tv.hour,":",
	        tv.minute,":",
	       tv.second,"\n");//,".",tv.fracSec);

	counter = 0;
	//while (counter < tabela.length) {
		//write(tabela[counter],"\t");
		//++counter;
	//}
	writeln("\n");
	wait(spawnShell("pause"));
}

